let collection = [];
// Write the queue functions below.

function enqueue(element) {

    collection[collection.length] = element
    return collection
}

function print() {
    return collection
}


function dequeue() {
    for(let x = 0; x < collection.length; x++){
        collection[x] = collection[x+1]
    }
    collection.length--;
    return collection
}

function front() {
    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
   if (collection.length === 0) {
        return true
   } else {
        return false
   }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
